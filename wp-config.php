<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vpnverbinding');

/** MySQL database username */
define('DB_USER', 'vpnverbinding_user');

/** MySQL database password */
define('DB_PASSWORD', 'vpn12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xP!*8,_XzU#dF:]#fWsCZ@V5uc2XZD6nZvOp*dbxRJF^e]{|1`~|D_-=gM#{1e.Z');
define('SECURE_AUTH_KEY',  '3Ccfs -!DgeuZ~m:a;0qy71Ho&oGg)1GRrB/]0%l|@bpKRn$Tnb{MxxMe|M(@&|T');
define('LOGGED_IN_KEY',    ']7a}dR}gL,mY)j8uX{pyg&4&Xen YY$Q|}*m9=(dx<4: CL_cDa+}AV4)f?tf_jt');
define('NONCE_KEY',        '%_8&$/G61>k5sX2rnCTqv2z?E&[nj1[Tb;fOJ,RBZ~]YGVi[mz t{C`n27H8&76O');
define('AUTH_SALT',        'lE5J*#x{J.mDIiO9j=%K_)k{;rEcW>{(gYCvVE~O#I8+{{+rgXAr|Bs;y]lrXmU{');
define('SECURE_AUTH_SALT', 'R*Zp0_W~df5X4+ag@`O*[[9xfO@|a_e4j3sH.c<7g6inN{x6/-74q}<kNmLc1!G8');
define('LOGGED_IN_SALT',   '05c^=6u!c/t;(AWVDniT>v/aEswL)T2ISO-S&sX/i5|-0mA2Q5GT&R#v~dqt1o,Q');
define('NONCE_SALT',       '3o(1[L!U1)Z3WWpFV`q7XkP%pF)=!7k%}[Y9@ q<}iO1N._ET$xP9a`|m=9K_S}$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
